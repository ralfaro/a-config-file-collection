# Import ready Firefox Bookmarks 

By importing this bookmark file you'll add the following custom searches

| Search  | Keyword  |Description   | 
|---|---|---|
|  GitLab Handbook | gl  | Handbook search   |
|  GitLab Documentation | gd |  GitLab documentation [ docs.gitlab.com] | 
| Issues by author  | author  | Search for issues open by given author (username) in all projects  |
| Merge Requests by author   | mr  |  Search for merge requests by given author (username) in all projects |
| Google Drive search  | dv  | Simple google drive search   |

# How to use

From Firefix go to the location bar ( or press Command L ) and type the keboard and the search keyword

### Examples
Search about tools in the handbook
``` 
gl tools
```

Search about unicorn in the docs
``` 
gd unicorn
```

Search merge requests opened by ronniealfaro
``` 
mr ronniealfaro
```
