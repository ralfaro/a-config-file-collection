# A config file collection

A collection of handy config files, focused on macos and useful bit and ends for GitLab work.

## Firefox

### GitLabSearchShortcuts.html
An import ready bookmark file with the custom and some advanced searches for GitLab

## Git

### gitignore_global_macos
Some good to have files in your global gitignore file

### My_config
My personal git config

## MacOs_Linux

### alias.sh
Some aliases, mostly for git.

### bash_promot_git_branch.sh
A very simple bash prompt which also shows the current branch you have checkout 

### zsh_prompt_git_branch
Same as the `bash_prompt_git_branch.sh` but for zsh